import urllib.request
import xml.etree.ElementTree
from pprint import pprint

SIGNOS = { '1':'Áries',
           '2':'Touro',
           '3':'Gêmeos',
           '4':'Câncer',
           '5':'Leão',
           '6':'Virgem',
           '7':'Libra',
           '8':'Escorpião',
           '9':'Sagitário',
          '10':'Capricórnio',
          '11':'Aquário',
          '12':'Peixes' }


with urllib.request.urlopen('http://www.toucharound.com/horoscopo/xml/daily.xml') as url:
    content = url.read()

root = xml.etree.ElementTree.fromstring(content)
print('Horóscopo para o dia {0}'.format(root.find('date').text))
print('Fonte: {0}'.format(root.find('source').text))

pprint({signo: texto for signo, texto in zip([SIGNOS[elem.attrib['id']] for elem in root.findall('sign')],
                                                        [elem.text.strip() for elem in root.findall('sign/message')])})
