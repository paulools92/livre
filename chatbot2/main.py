from chatterbot.trainers import ListTrainer
from chatterbot import ChatBot
import cotacao as cot
from datetime import datetime, timedelta
from datetime import date
global typeSpeed

typeSpeed = 0.12
cotacao = cot.Cotacao()



bot = ChatBot('Chat Bot')

conversa = ['Oi', 'Olá', 'Tudo bem?', 'Tudo ótimo', 'Você gosta de programar?', 'Sim, eu programo em Python', 'Que time você torce', 'Torço apenas para o Brasil', 'O que você faz da vida?', 'Eu estudo','O que gosta de faze no tempo livre?', 'Eu gosto de andar de patins', 'Qual seu cor favorita','Preto','legal', 'Bacana', 'Qual a cotação do dolar hoje?', ('Dolar: R${}'.format(cotacao.dolar())),'obrigado', 'Imigina', 'Qual a cotação do euro hoje?', ('Euro: R${}'.format(cotacao.euro())), 'Qual a cotação da libra hoje?',('Libra: R${}'.format(cotacao.libra())),'Qual a cotação do bitcoin hoje?', ('Bitcoin: R${}'.format(cotacao.bitcoin())), 'Qual seu nome?', 'Meu nome é José', 'Qual sua idade?', 'Eu tenho 25 anos','Qual sua idade?', 'Tenho 25 e você?', 'Você tem gatos?', 'tenho dois', 'interessante', 'hum.. verdade', 'Será que vai chover?', 'melhor não arriscar né?', 'meu brother', 'tamo junto', 'Tchau', 'tchau...' , 'Bye', 'ByeBye', 'Até logo', 'Até breve', 'Adeus', 'Espero que isso não seja um Deus, e sim um até logo', 'Até mais meu amigo','Até breve', 'Przer josé', 'Prazer é todo meu', 'Bom dia', 'Bom dia meu aimgo', 'Boa tarde', 'Boa tarde meu amigo', 'Boa noite', 'Boa noite meu amigo']

bot.set_trainer(ListTrainer)
bot.train(conversa)

while True:
    pergunta = input("Você: ")
    resposta = bot.get_response(pergunta)
    if float(resposta.confidence) > 0.5:
        print('Bot: ', resposta)
    else:
        print('Bot: Ainda não sei responder esta pergunta')
