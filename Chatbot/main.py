# -*- coding: utf-8 -*-

from chatterbot.trainers import ListTrainer #Treinador
from chatterbot import ChatBot #chatbot
import os

bot = ChatBot('Chatbot')

bot.set_trainer(ListTrainer)

for arq in os.listdir('arq'):
    chats = open('arq/'+ arq,'r').readlines()
    bot.train(chats)
    
while True:
    resq = input('Você: ')

    resp = bot.get_response(resq)
    print('Bot: '+ str(resp))

